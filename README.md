# README #

Personio Employee Service

## Overview ##

### Quick summary 

This is the employee service of Personio company, designed based on the requirements of the interview assignment.

### Requirements

  Available in this [file](documents/requirement.pdf).

### Design documents

  Available in this [folder](documents/designs).

   - High-level design: [link](documents/designs/high_level_design.jpg)

   - Model design: [link](documents/designs/data_model.jpg)

   - Service Architecture design: [link](documents/designs/service_architecture.jpg)

   - Rest API design: [link](swagger.yaml)

### Architecture

  The service is built using the architect: Hexagonal Architecture + Onion Architecture.
  The architecture details can be found [here](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/)

### Project structure

```
|docker                Docker files to build related services.
|documents             Documents of the project.
|src                   The project source based.
|--|infrastructure     Facilitate connections of the application core to tools like a database, 3rd API.
|--|--|database        Database tool
|--|--|--|adapters     Adapters to database of each modules in the application core
|--|interface          Interfaces of our application, i.e: REST API, CLI,...
|--|--|api             Express REST API interface
|--|--|--|adapters     Adapters (i.e controllers) to Rest API of each modules in the application core
|--|--|--|middleware   Middlewares using by Express
|--|--|--|utilities    Utilities to support Express
|--|libs               Tools/Infra that support the application working, i.e: AppErrors, Http, utilities,...
|--|modules            Application core of project
|--|--|user            Represent a user concept in the application
|--|--|--|application  Application layer of user, contains application service and ports for outside world
|--|--|--|domain       Domain layer of user, contains core business logic of user
|--|--|hierarchy       Represent a hierarchy concept in the application
|--|--|--|application  Application layer of hierarchy, contains application service and ports for outside world
|--|--|--|domain       Domain layer of hierarchy, contains core business logic of hierarchy
```

## How to build and run the service? 

# Prerequisite

- Docker v20.10 installed.

- Port 80 is not acquired.

### Build

  At the root of the project, run the following command

    docker-compose -f docker-compose.dev.yaml build
    
### Run
  At the root of the project, run the following command

    docker-compose -f docker-compose.dev.yaml up -d

  Wait until all docker containers started

  Service endpoint is available at this [link](http://localhost/personio/v1/)

  Swagger UI for service is available at [link](http://localhost/personio/swagger/)