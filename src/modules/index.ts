import { registerHierarchyDependencies } from './hierarchy';
import { registerUserDependencies } from './user';

/**
 * Register application dependencies to IoC container
 */
export function registerApplicationDependencies() {
  registerUserDependencies();
  registerHierarchyDependencies();
}