import { container } from 'tsyringe';
import { IUserService, UserServiceToken, } from './application/userService.port';

import {
  UnauthorizedUserError,
  UserExistsError, UserNotFoundError
} from './application/userApplicationErrors';
import { IUserRepository, UserRepositoryToken } from './application/userRepository.port';
import { UserService } from './application/userService';
import { User } from './domain/user';

/**
 * Register user dependencies to IoC container
 */
function registerUserDependencies() {
  container.register<IUserService>(UserServiceToken, { useClass: UserService });
}

export {
  User, registerUserDependencies, IUserService, IUserRepository,
  UserServiceToken, UserRepositoryToken,
  UserExistsError, UserNotFoundError, UnauthorizedUserError,
};