import { inject, injectable } from 'tsyringe';
import { User } from '../domain/user';
import { IUserRepository, UserRepositoryToken } from './userRepository.port';
import {
  UnauthorizedUserError,
  UserExistsError, UserNotFoundError
} from './userApplicationErrors';
import { IUserService } from './userService.port';

@injectable()
export class UserService implements IUserService {
  private userRepository: IUserRepository;

  constructor(@inject(UserRepositoryToken) userRepository: IUserRepository) {
    this.userRepository = userRepository;
  }

  /**
   * @inheritdoc @see {@link IUserService}
   */
  public async createUser(userName: string, password: string): Promise<void> {
    const existingUser = await this.userRepository.findUser(userName);

    if (existingUser) {
      throw new UserExistsError(`User with username ${userName} already exists.`);
    }

    const user = new User(userName, password);
    await user.encryptPassword();
    await this.userRepository.createUser(user);
  }

  /**
  * @inheritdoc {@link IUserService}
  */
  public async authenticateUser(userName: string, password: string): Promise<User> {
    const existingUser = await this.userRepository.findUser(userName);

    if (!existingUser) {
      throw new UserNotFoundError(`User with ${userName} is not found.`);
    }

    if (!await existingUser.verifyRawPassword(password)) {
      throw new UnauthorizedUserError('User password is incorrect.');
    }

    return existingUser;
  }
}