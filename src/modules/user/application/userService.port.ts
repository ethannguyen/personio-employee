import { User } from '../domain/user';

/**
 * A token used to register {@link IUserService} to IoC container
 */
export const UserServiceToken = "IUserService";

/**
 * An application service for user domain
 */
export interface IUserService {
  /**
   * Create an user with input parameters
   * @param userName userName of user
   * @param password password of user, used for authentication
   * @throws {@link UserExistsError} if user already exists
   */
  createUser(userName: string, password: string): Promise<void>;

  /**
   * Authenticate the user with userName and password
   * @param userName userName of user
   * @param password password of user, used to authenticate user
   * @throws {@link UserNotExistError} if user doesn't exist
   * @throws {@link UserUnauthorizedError} if user password is not correct
   */
  authenticateUser(userName: string, password: string): Promise<User>;
}