import { User } from '../domain/user';

export const UserRepositoryToken = 'IUserRepository';

/**
 * A repository used to CRUD {@link User} domain
 */
export interface IUserRepository {
  /**
   * Find a {@link User} with the input userName
   * @param userName userName of the user to being find
   * @returns An user instance otherwise undefined if not found.
   */
  findUser(userName: string): Promise<User | undefined>

  /**
   * Create a user with input {@link User} domain
   * @param user new user to be created
   * @throws error if the userName of user already exists
   */
  createUser(user: User): Promise<void>;
}