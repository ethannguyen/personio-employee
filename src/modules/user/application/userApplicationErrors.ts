import { AppError } from 'src/libs/error';
import { ModulesErrorCodeRanges } from 'src/modules/modulesErrorCodeRanges';

/**
 * Error codes for users concepts in application layer
 */
class UserApplicationErrorCodes {
  /**
   * User already exists error code
   */
  public static readonly ERROR_CODE_USER_EXISTS =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_USER + 0;

  /**
   * User already exists error name
   * @see {@link ERROR_CODE_USER_EXISTS}
   */
  public static readonly ERROR_NAME_USER_EXISTS = 'User already exists.';

  /**
  * User not found error code
  */
  public static readonly ERROR_CODE_USER_NOT_FOUND =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_USER + 1;

  /**
   * User not found error name
   * @see {@link ERROR_CODE_USER_NOT_FOUND}
   */
  public static readonly ERROR_NAME_USER_NOT_FOUND = 'User not founds.';

  /**
  * User is not authorized error code
  */
  public static readonly ERROR_CODE_USER_NOT_AUTHORIZED =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_USER + 2;

  /**
   * User is not authorized error name
   * @see {@link ERROR_CODE_USER_NOT_AUTHORIZED}
   */
  public static readonly ERROR_NAME_USER_NOT_AUTHORIZED = 'User is not authorized.';
}

/**
 * User already exists error
 */
export class UserExistsError extends AppError {
  constructor(message: string) {
    super(UserApplicationErrorCodes.ERROR_CODE_USER_EXISTS,
      UserApplicationErrorCodes.ERROR_NAME_USER_EXISTS,
      message);
  }
}

/**
 * User not found error
 */
export class UserNotFoundError extends AppError {
  constructor(message: string) {
    super(UserApplicationErrorCodes.ERROR_CODE_USER_NOT_FOUND,
      UserApplicationErrorCodes.ERROR_NAME_USER_NOT_FOUND,
      message);
  }
}


/**
 * User is not authorized.
 */
export class UnauthorizedUserError extends AppError {
  constructor(message: string) {
    super(UserApplicationErrorCodes.ERROR_CODE_USER_NOT_AUTHORIZED,
      UserApplicationErrorCodes.ERROR_NAME_USER_NOT_AUTHORIZED,
      message);
  }
}