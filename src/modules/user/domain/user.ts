import bcrypt from 'bcrypt';

/**
 * Represent a user domain concept.
 * A user is a client of the application. 
 */
export class User {
  /**
   * Number of rounds using to hash password using bcrypt
   */
  private static BCRYPT_SALT_ROUNDs = 10;

  /**
   * UserName of user, used as identifier.
   */
  public userName: string;

  /**
   * Password used for authentication.
   */
  public password: string;

  constructor(userName: string, password: string) {
    this.userName = userName;
    this.password = password;
  }

  /**
   * Encrypt the raw password of this application.
   * The current password will be replaced by the encrypted password
   * @remarks Make user the current password is not encrypted before use.
   */
  public async encryptPassword(): Promise<void> {
    const encryptedPassword = await bcrypt.hash(this.password, User.BCRYPT_SALT_ROUNDs);
    this.password = encryptedPassword;
  }

  /**
   * Verify whether the input raw password match with encrypted password of this user.
   * @param rawPassword Raw password to be compared
   * @returns true if password match, otherwise false
   */
  public verifyRawPassword(rawPassword: string): Promise<boolean> {
    return bcrypt.compare(rawPassword, this.password);
  }
}
