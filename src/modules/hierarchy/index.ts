import { container } from 'tsyringe';
import { HierarchyService } from './application/hierarchyService';
import { IHierarchyService, HierarchyServiceToken } from './application/hierarchyService.port';
import { IHierarchyRepository, HierarchyRepositoryToken } from './application/hierarchyRepository.port';
import { HierarchyNotFoundError, EmployeeNotFoundInHierarchyError } from './application/hierarchyApplicationErrors';
import { InvalidHierarchyError } from './domain/hierarchyDomainErrors';
import { Hierarchy } from './domain/hierarchy';
import { Employee } from './domain/employee';

/**
 * Register hierarchy dependencies to IoC container
 */
function registerHierarchyDependencies() {
  container.register<IHierarchyService>(HierarchyServiceToken, { useClass: HierarchyService });
}

export {
  Hierarchy, Employee,
  registerHierarchyDependencies, IHierarchyService, HierarchyServiceToken,
  IHierarchyRepository, HierarchyRepositoryToken,
  InvalidHierarchyError, HierarchyNotFoundError, EmployeeNotFoundInHierarchyError,
};