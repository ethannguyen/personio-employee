import { inject, injectable } from 'tsyringe';
import { IHierarchyService } from './hierarchyService.port';
import {
  HierarchyRepositoryToken,
  IHierarchyRepository
} from './hierarchyRepository.port';
import { Employee } from '../domain/employee';
import { Hierarchy } from '../domain/hierarchy';
import { EmployeeNotFoundInHierarchyError } from './hierarchyApplicationErrors';

@injectable()
export class HierarchyService implements IHierarchyService {
  private hierarchyRepository: IHierarchyRepository;

  constructor(@inject(HierarchyRepositoryToken) hierarchyRepository: IHierarchyRepository) {
    this.hierarchyRepository = hierarchyRepository
  }

  /**
   * @inheritdoc {@link IHierarchyService}
   */
  public async create(inputEmployees: Employee[]): Promise<Hierarchy> {
    const hierarchy = Hierarchy.create(inputEmployees);
    await this.hierarchyRepository.createHierarchy(hierarchy);
    return hierarchy
  }

  /**
   * @inheritdoc {@link IHierarchyService}
   */
  public async getEmployeeInHierarchy(employeeName: string, supervisorDepth: number): Promise<Employee> {
    const hierarchy = await this.hierarchyRepository.getCurrentHierarchy();
    const employee = hierarchy.getEmployee(employeeName, supervisorDepth);

    if (!employee) {
      throw new EmployeeNotFoundInHierarchyError(`The employee ${employeeName} is not found in the hierarchy.`);
    }

    return employee;
  }
}