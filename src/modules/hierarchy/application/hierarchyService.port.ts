import { Employee } from '../domain/employee';
import { Hierarchy } from '../domain/hierarchy';

export const HierarchyServiceToken = 'IHierarchyService';

/**
 * An application service for hierarchy domain
 */
export interface IHierarchyService {
  /**
   * Create a hierarchy
   * @param inputEmployees All employees inside the hierarchy.
   * @remarks In inputEmployees, Name of employee is not necessary unique. 
   * But combination of employee name and supervisor should be unique, otherwise, duplicated ones will be ignored.
   */
  create(inputEmployees: Employee[]): Promise<Hierarchy>;

  /**
   * Get an employee with its supervisor based on given input
   * @param employeeName name of employee
   * @param supervisorDepth depth level to get the employee supervisor
   * @throws {@link HierarchyNotFoundError} if the hierarchy is not found
   * @throws {@link EmployeeNotFoundInHierarchyError} if the employee is not found in the hierarchy
   */
  getEmployeeInHierarchy(employeeName: string, supervisorDepth: number): Promise<Employee>;
}