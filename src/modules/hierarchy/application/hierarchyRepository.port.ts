import { Hierarchy } from '../domain/hierarchy';

export const HierarchyRepositoryToken = 'IHierarchyRepository';

export interface IHierarchyRepository {
  /**
   * Create a new hierarchy
   * @param hierarchy hierarchy to be created
   */
  createHierarchy(hierarchy: Hierarchy): Promise<void>;

  /**
   * Get current hierarchy
   * @throws 
   */
  getCurrentHierarchy(): Promise<Hierarchy>;
}