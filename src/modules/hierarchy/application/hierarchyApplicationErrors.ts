import { AppError } from 'src/libs/error';
import { ModulesErrorCodeRanges } from 'src/modules/modulesErrorCodeRanges';

/**
 * Error codes for hierarchy domain concept in application layer
 */
export class HierarchyApplicationErrorCodes {
  /**
    * Employee is not found in the hierarchy error code
    */
  public static readonly ERROR_CODE_EMPLOYEE_NOT_FOUND_IN_HIERARCHY =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_HIERARCHY + 0;

  /**
   * Employee is not found in the hierarchy error name
   * @see {@link ERROR_CODE_EMPLOYEE_NOT_FOUND_IN_HIERARCHY}
   */
  public static readonly ERROR_NAME_EMPLOYEE_NOT_FOUND_IN_HIERARCHY = 'The employee is not found in the hierarchy.';

  /**
   * Hierarchy not found error code
   */
  public static readonly ERROR_CODE_HIERARCHY_NOT_FOUND =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_HIERARCHY + 1;

  /**
   * Hierarchy not found error name
   * @see {@link ERROR_CODE_HIERARCHY_NOT_FOUND}
   */
  public static readonly ERROR_NAME_HIERARCHY_NOT_FOUND = 'Hierarchy not found.';
}

/**
 *  Employee is not found in the hierarchy error
 */
export class EmployeeNotFoundInHierarchyError extends AppError {
  constructor(message: string) {
    super(
      HierarchyApplicationErrorCodes.ERROR_CODE_EMPLOYEE_NOT_FOUND_IN_HIERARCHY,
      HierarchyApplicationErrorCodes.ERROR_NAME_EMPLOYEE_NOT_FOUND_IN_HIERARCHY,
      message);
  }
}

/**
 * Hierarchy not found error
 */
export class HierarchyNotFoundError extends AppError {
  constructor(message?: string) {
    super(
      HierarchyApplicationErrorCodes.ERROR_CODE_HIERARCHY_NOT_FOUND,
      HierarchyApplicationErrorCodes.ERROR_NAME_HIERARCHY_NOT_FOUND,
      message || HierarchyApplicationErrorCodes.ERROR_NAME_HIERARCHY_NOT_FOUND);
  }
}