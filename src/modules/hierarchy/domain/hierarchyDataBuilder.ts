import _ from 'lodash';
import { ErrorStack } from 'src/libs/error';
import { Employee } from './employee';
import { HierarchyDomainErrorCodes, InvalidHierarchyError } from './hierarchyDomainErrors';

export class HierarchyDataBuilder {
  private employees: Employee[] = [];
  private employeeSuperviseeMap: Map<string, Employee[]> = new Map();

  constructor(employees: Employee[]) {
    this.employees = employees;
  }

  /**
   * Build data for hierarchy from the input
   * @returns Necessary data to create a new hierarchy
   * @throws Error if the input data is invalid.
   * Input data is invalid if:
   * - Contains multiple hierarchies
   * - Contains no head employee
   * - Contains loop paths
   * - Contains any employee with multiple supervisors
   */
  public build(): { headEmployee: Employee, employeeSuperviseeMap: Map<string, Employee[]> } {
    const uniqEmployees = _.uniqWith(this.employees, (employeeA, employeeB) =>
      employeeA.name == employeeB.name && employeeA.supervisor?.name == employeeB.supervisor?.name);
    this.employeeSuperviseeMap = this.createEmployeeSuperviseeMap(this.employees);
    const headEmployee = this.validateHierarchies(uniqEmployees);
    return { headEmployee, employeeSuperviseeMap: this.employeeSuperviseeMap };
  }

  private validateHierarchies(employees: Employee[]): Employee {
    const uniqueEmployees = _.uniqBy(employees, employee => employee.name);
    const possibleHeadEmployees = employees.filter(e => e.supervisor === undefined);
    const visitedHierarchies: Employee[][] = [];
    const employeePaths: Path[] = [];

    do {
      const unvisitedEmployees = _.differenceBy(uniqueEmployees, _.flatten(visitedHierarchies), 'name');
      const currentVisitedEmployees: Employee[] = [];
      const rootEmployee = _.intersectionBy(unvisitedEmployees, possibleHeadEmployees, 'name')[0] || unvisitedEmployees[0];
      employeePaths.push(...this.validateHierarchy(rootEmployee, currentVisitedEmployees));
      let isHierarchyAdded = false;

      // Detect if current hierarchy is already visited and replace if current one has a longer path.
      visitedHierarchies.forEach((visitedEmployees, index) => {
        const isSameHierarchy = _.differenceBy(visitedEmployees, currentVisitedEmployees).length === 0;

        if (isSameHierarchy) {
          visitedHierarchies[index] = currentVisitedEmployees;
          isHierarchyAdded = true;
          return;
        }
      });

      if (!isHierarchyAdded) {
        visitedHierarchies.push(currentVisitedEmployees)
      }

    } while (_.flatten(visitedHierarchies).length < uniqueEmployees.length);

    this.throwErrorIfInvalid(visitedHierarchies, possibleHeadEmployees, employeePaths);
    return possibleHeadEmployees[0];
  }

  private throwErrorIfInvalid(visitedHierarchies: Employee[][],
    possibleHeadEmployees: Employee[], employeePaths: Path[]) {

    const errorStack: ErrorStack = [];

    if (visitedHierarchies.length > 1) {
      errorStack.push({
        code: HierarchyDomainErrorCodes.ERROR_CODE_MULTIPLE_HIERARCHIES,
        name: HierarchyDomainErrorCodes.ERROR_NAME_MULTIPLE_HIERARCHIES,
        detail: `There are ${visitedHierarchies.length} hierarchies`,
      });
    }

    if (possibleHeadEmployees.length === 0) {
      errorStack.push({
        code: HierarchyDomainErrorCodes.ERROR_CODE_NO_HEAD_EMPLOYEE,
        name: HierarchyDomainErrorCodes.ERROR_NAME_NO_HEAD_EMPLOYEE,
        detail: HierarchyDomainErrorCodes.ERROR_NAME_NO_HEAD_EMPLOYEE,
      });
    }

    const loopPaths = employeePaths.filter(path => path.isLoop);
    const loopErrors = _.uniqBy(this.getLoopErrorStack(loopPaths), (error) => error.detail);
    const multipleRootsErrors = this.getMultipleRootsErrorStack(employeePaths);
    errorStack.push(...loopErrors);
    errorStack.push(...multipleRootsErrors);

    if (errorStack.length > 0) {
      throw new InvalidHierarchyError(errorStack);
    }
  }

  private validateHierarchy(headEmployee: Employee, visitedNodes: Employee[]): Path[] {
    const employeePaths: Path[] = [{ employees: [], isLoop: false }];
    this.traversalAllEmployees(headEmployee, employeePaths, visitedNodes);
    return employeePaths;
  }

  private createEmployeeSuperviseeMap(employees: Employee[]) {
    const employeeSuperviseeMap = new Map<string, Employee[]>();
    const employeeSet = _.uniqBy(employees, (employee) => employee.name);

    employeeSet.forEach(employee => {
      employeeSuperviseeMap.set(employee.name, []);
    });

    employees.forEach(employee => {
      if (employee.supervisor) {
        const supervisees = employeeSuperviseeMap.get(employee.supervisor.name);
        //@ts-ignore At this point, supervisees always available.
        if (!supervisees.find(supervise => supervise.name === employee.name)) {
          supervisees!.push(employee);
        }
      }
    })

    return employeeSuperviseeMap;
  }

  /**
  * Traversal all employees in the hierarchy and update paths and visited employees
  * @param employee Employee to be checked.
  * @param paths All the paths in hierarchy. A path is a list of employees
  * from head employee to employee which contains no supervisee.
  * @param visitedEmployees A list contains on visited employees while traversing
  */
  private traversalAllEmployees(employee: Employee, paths: Path[], visitedEmployees: Employee[]) {
    const currentPathIndex = paths.length - 1;
    let currentPath = paths[currentPathIndex];
    const isEmployeeVisited = visitedEmployees
      .some(visitedEmployee => visitedEmployee.name === employee.name);

    if (isEmployeeVisited) {
      currentPath.isLoop = currentPath.employees
        .some(pathEmployee => pathEmployee.name === employee.name);

      if (!currentPath.isLoop) {
        // Continue the path to find the possible loop path
        var newPath = {
          employees: [...currentPath.employees],
          isLoop: currentPath.isLoop,
          containEmployeeInOtherPaths: false
        }
        currentPath.employees.push(employee);
        currentPath = newPath;
        paths.push(currentPath);
      } else {
        currentPath.employees.push(employee);
        return paths;
      }
    }

    if (!isEmployeeVisited) {
      visitedEmployees.push(employee);
    }

    currentPath.employees.push(employee);
    const ancestorEmployees = [...currentPath.employees];
    const supervisees = this.employeeSuperviseeMap.get(employee.name) || [];

    supervisees.forEach((supervisee, index) => {
      this.traversalAllEmployees(supervisee, paths, visitedEmployees);
      if (index !== supervisees.length - 1) {
        paths.push({
          employees: [...ancestorEmployees],
          isLoop: false
        });
      }
    });
  }

  private getLoopErrorStack(loopPaths: Path[]): ErrorStack {
    return loopPaths.map(path => {
      const { employees } = path;
      const employeeNames = this.toEmployeeNames(employees);
      const duplicatedEmployeeLookup = this.getDuplicatedEmployeeLookup(employeeNames);
      const duplicatedEmployeeName = employeeNames.filter(employee => duplicatedEmployeeLookup[employee])[0]

      const duplicatedEmployeeIndexes = this.getIndexesOfItem(employeeNames, duplicatedEmployeeName);

      const loopEmployees = employees.slice(Math.min(...duplicatedEmployeeIndexes),
        Math.max(...duplicatedEmployeeIndexes) + 1);
      const errorMessage = `Loop path: ${this.toEmployeeNames(loopEmployees).join(' -> ')}`;
      return {
        code: HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH,
        name: HierarchyDomainErrorCodes.ERROR_NAME_HIERARCHY_CONTAINS_LOOP_PATH,
        detail: errorMessage,
      }
    });
  }

  private getMultipleRootsErrorStack(employeePaths: Path[]): ErrorStack {
    const lastEmployeeInPaths = employeePaths.map(path => {
      const { employees } = path;
      const lastEmployee = new Employee(employees[employees.length - 1].name);
      lastEmployee.supervisedBy(employees[employees.length - 2]);
      return lastEmployee;
    });

    const employeeWithMultipleSupervisors = new Map<string, string[]>();

    lastEmployeeInPaths.forEach(employee => {
      const employeeName = employee.name;

      if (employeeWithMultipleSupervisors.has(employeeName)) {
        return;
      }

      const supervisors =
        _.uniq(this.findSupervisorNamesInOtherPaths(employeePaths, employeeName));

      if (supervisors.length <= 1) {
        return;
      }

      employeeWithMultipleSupervisors.set(employeeName, supervisors);
    });

    const errorStack: ErrorStack = [];

    employeeWithMultipleSupervisors.forEach((supervisors, employeeName) => {
      const errorMessage = 'There is an employee has multiple supervisors: '
        + supervisors.map(supervisor => `${supervisor} -> ${employeeName}`).join(', ');

      errorStack.push({
        code: HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_MULTIPLE_SUPERVISORS,
        name: HierarchyDomainErrorCodes.ERROR_NAME_HIERARCHY_MULTIPLE_SUPERVISORS,
        detail: errorMessage,
      })
    });

    return errorStack;
  }

  private findSupervisorNamesInOtherPaths(paths: Path[], employeeName: string): string[] {
    return paths.reduce((acc, path) => {
      const employeeNames = this.toEmployeeNames(path.employees);
      const duplicatedEmployeeIndexes = this.getIndexesOfItem(employeeNames, employeeName);

      duplicatedEmployeeIndexes.filter(index => index > 0).forEach(index => {
        acc.push(employeeNames[index - 1]);
      })
      return acc;
    }, [] as string[]);
  }

  private getDuplicatedEmployeeLookup(employeeNames: string[]): DuplicatedEmployeeLookup {
    return employeeNames.reduce((acc, employee) => {
      acc[employee] = ++acc[employee] || 0;
      return acc;
    }, {} as DuplicatedEmployeeLookup);
  }

  private getIndexesOfItem(array: string[], targetItem: string) {
    return Object.keys(
      _.pickBy(array, (item) => item === targetItem))
      .map(index => +index);
  }

  private toEmployeeNames(employees: Employee[]): string[] {
    return employees.map(employee => employee.name);
  }
}

/**
 * A path is a list of employees from head employee to employee which contains no supervisee.
 * A path also contains indicators to indicate if the path is looped or contains employee in other paths
 */
interface Path {
  employees: Employee[];
  isLoop: boolean;
}

interface DuplicatedEmployeeLookup {
  [key: string]: number;
}