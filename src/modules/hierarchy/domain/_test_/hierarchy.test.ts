import { Employee } from '../employee';
import { Hierarchy } from '../hierarchy';
import { HierarchyDomainErrorCodes, InvalidHierarchyError } from '../hierarchyDomainErrors';

describe('Create hierarchy', () => {
  it('Throws hierarchy contains loop error if there is no head employee in the input', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '2'),
      createEmployeeSupervisorPair('1', '3'),
    ];

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(2);
      expect(invalidError.errorStack![0].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_NO_HEAD_EMPLOYEE);
      expect(invalidError.errorStack![1].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH);
    }
  });

  it('Throws error if there is more than one hierarchy', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('4', '3'),
      createEmployeeSupervisorPair('1'),
      createEmployeeSupervisorPair('3'),
    ];

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(1);
      expect(invalidError.errorStack![0].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_MULTIPLE_HIERARCHIES);
    }
  });


  it('Throws loop path errors if the input data contains loop paths', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '2'),
      createEmployeeSupervisorPair('4', '3'),
      createEmployeeSupervisorPair('5', '3'),
      createEmployeeSupervisorPair('2', '4'),
      createEmployeeSupervisorPair('6', '5'),
      createEmployeeSupervisorPair('2', '6'),
      createEmployeeSupervisorPair('1')
    ];

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(3);
      expect(invalidError.errorStack![0].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH);
      expect(invalidError.errorStack![1].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH);
      expect(invalidError.errorStack![2].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_MULTIPLE_SUPERVISORS);
    }
  });

  it('Throws multiple supervisors errors if there is an employee has more than one supervisor', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '1'),
      createEmployeeSupervisorPair('4', '1'),
      createEmployeeSupervisorPair('5', '2'),
      createEmployeeSupervisorPair('5', '3'),
      createEmployeeSupervisorPair('5', '4'),
      createEmployeeSupervisorPair('1'),
    ]

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(1);
      expect(invalidError.errorStack![0].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_HIERARCHY_MULTIPLE_SUPERVISORS);
    }
  });

  it('Throw multiple errors with invalid hierarchy', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '1'),
      createEmployeeSupervisorPair('2', '3'),
      createEmployeeSupervisorPair('3', '2'),
      createEmployeeSupervisorPair('4'),
      createEmployeeSupervisorPair('2', '4'),
      createEmployeeSupervisorPair('3', '4'),
      createEmployeeSupervisorPair('1'),
    ]

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(5);
    }
  });

  it('Throw errors if input data contains multiple hierarchies and one of them is looped', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('4', '3'),
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '2'),
      createEmployeeSupervisorPair('1', '1'),
      createEmployeeSupervisorPair('A', 'B'),
      createEmployeeSupervisorPair('B'),
    ];

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack?.length).toEqual(2);
    }
  });

  it('Throw errors if the input contains multiple head employee', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('A', 'B'),
      createEmployeeSupervisorPair('B'),
      createEmployeeSupervisorPair('C', 'D'),
      createEmployeeSupervisorPair('D'),
      createEmployeeSupervisorPair('E', 'F'),
      createEmployeeSupervisorPair('F'),
    ]

    try {
      Hierarchy.create(input);
      expect(true).toBeFalsy();
    } catch (err) {
      expect(err).toBeInstanceOf(InvalidHierarchyError);
      const invalidError = err as InvalidHierarchyError;
      expect(invalidError.errorStack![0].code).toEqual(HierarchyDomainErrorCodes.ERROR_CODE_MULTIPLE_HIERARCHIES);
    }
  });

  it('Successfully if the input data is valid', () => {
    const input: Employee[] = [
      createEmployeeSupervisorPair('2', '1'),
      createEmployeeSupervisorPair('3', '2'),
      createEmployeeSupervisorPair('4', '3'),
      createEmployeeSupervisorPair('1'),
      createEmployeeSupervisorPair('5', '3'),
      createEmployeeSupervisorPair('6', '2'),
      createEmployeeSupervisorPair('7', '6'),
      createEmployeeSupervisorPair('9', '1'),
      createEmployeeSupervisorPair('10', '9'),
      createEmployeeSupervisorPair('11', '1'),
    ]

    const hierarchy = Hierarchy.create(input);
    expect(hierarchy).not.toBeUndefined();
    expect(hierarchy.headEmployee.name).toEqual('1');
  });
});


function createEmployeeSupervisorPair(employeeName: string, supervisorName?: string): Employee {
  const employee = new Employee(employeeName);
  if (supervisorName) {
    const supervisor = new Employee(supervisorName);
    employee.supervisedBy(supervisor);
  }
  return employee;
}