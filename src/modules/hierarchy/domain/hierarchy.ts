import _ from 'lodash';
import { Employee } from './employee';
import { HierarchyDataBuilder } from './hierarchyDataBuilder';

/**
 * Represent an hierarchy of a company
 */
export class Hierarchy {
  /**
   * Head employee of in the hierarchy, stand on top of the hierarchy
   */
  public headEmployee: Employee;

  /**
   * A map contains relationship between employee and supervisees
   */
  private employeeSuperviseeMap: Map<string, Employee[]> = new Map();

  /**
   * List of all employees in the hierarchy;
   */
  private employees: Employee[] = [];

  /**
   * Create a hierarchy
   * @param employees Employees of the hierarchy
   * @returns An valid hierarchy
   * @remarks A hierarchy is considered invalid if:
   * - There is any supervisors loop inside.
   * - There is any employee which is supervised by more than one supervisor.
   * - There are multiple head employees
   * @throws {@link InvalidHierarchyError} if the hierarchy is invalid.
   */
  public static create(employees: Employee[]) {
    const hierarchyDataBuilder = new HierarchyDataBuilder(employees);
    const { headEmployee, employeeSuperviseeMap } = hierarchyDataBuilder.build();
    const hierarchy = new Hierarchy(headEmployee, employeeSuperviseeMap)
    return hierarchy;
  }

  private constructor(headEmployee: Employee, employeeSuperviseeMap: Map<string, Employee[]>) {
    this.headEmployee = this.buildHierarchy(headEmployee, employeeSuperviseeMap);
  }

  /**
   * Get supervisees of the input employee.
   * @param employee used to query supervisees 
   */
  public getSupervisees(employee: Employee): Employee[] {
    return this.employeeSuperviseeMap.get(employee.name)?.map(employee => employee.deepClone())
      || [];
  }

  public getEmployeesFromTopToBottom() {
    return this.getSuperviseesRecursive(this.headEmployee.deepClone());
  }

  public getEmployee(employeeName: string, supervisorDepth: number): Employee | undefined {
    const employee = this.employees.find(e => e.name === employeeName);

    if (!employee) {
      return undefined;
    }

    const clonedEmployee = employee.deepClone();
    let currentEmployee: Employee | undefined = clonedEmployee;
    let depth = 1;

    while (currentEmployee?.supervisor !== undefined && depth++ <= supervisorDepth) {
      currentEmployee = currentEmployee.supervisor;
    }

    currentEmployee.supervisedBy(undefined);
    return clonedEmployee;
  }

  private buildHierarchy(employee: Employee, employeeSuperviseeMap: Map<string, Employee[]>) {
    const updatedEmployee = new Employee(employee.name);
    this.employees.push(updatedEmployee);
    const supervisees = employeeSuperviseeMap.get(employee.name) || [];

    const updatedSupervisees = supervisees.map(supervisee => {
      const updatedEmployee = this.buildHierarchy(supervisee, employeeSuperviseeMap);
      updatedEmployee.supervisedBy(employee);
      return updatedEmployee;
    });

    this.employeeSuperviseeMap.set(employee.name, updatedSupervisees);
    return updatedEmployee;
  }

  private getSuperviseesRecursive(employee: Employee, employeeList?: Employee[]): Employee[] {
    let newEmployeeList = [...employeeList || [], employee];
    const supervisees = this.getSupervisees(employee);

    supervisees.forEach(supervisee => {
      newEmployeeList = this.getSuperviseesRecursive(supervisee, newEmployeeList);
    });

    return newEmployeeList;
  }
}