/**
 * Represent an employee in the company
 */
export class Employee {

  private _supervisor?: Employee;

  /**
   * Name of the employee
   */
  public readonly name: string;

  /**
   * A supervisor of the employee
   */
  public get supervisor(): Employee | undefined {
    return this._supervisor;
  }

  constructor(name: string) {
    this.name = name;
  }

  public supervisedBy(employee: Employee | undefined) {
    this._supervisor = employee;
  }

  public deepClone() {
    const employee = new Employee(this.name);

    if (this.supervisor) {
      employee.supervisedBy(this.supervisor.deepClone());
    }

    return employee;
  }
}