import { AppError, ErrorStackObject } from 'src/libs/error';
import { ModulesErrorCodeRanges } from 'src/modules/modulesErrorCodeRanges';

/**
 * Error codes for hierarchy domain concept in domain layer
 */
export class HierarchyDomainErrorCodes {
  /**
    * Invalid hierarchy error code
    */
  public static readonly ERROR_CODE_INVALID_HIERARCHY_CODE =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY + 0;

  /**
   * Invalid hierarchy error name
   * @see {@link ERROR_CODE_INVALID_HIERARCHY_CODE}
   */
  public static readonly ERROR_NAME_INVALID_HIERARCHY = 'The hierarchy is invalid.';

  /**
    * No head employee in hierarchy error code
    */
  public static readonly ERROR_CODE_NO_HEAD_EMPLOYEE =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY + 1;

  /**
   * No head employee in hierarchy error name
   * @see {@link ERROR_CODE_NO_HEAD_EMPLOYEE}
   */
  public static readonly ERROR_NAME_NO_HEAD_EMPLOYEE = 'No head employee in the hierarchy.';

  /**
   * Multiple hierarchies error code
   */
  public static readonly ERROR_CODE_MULTIPLE_HIERARCHIES =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY + 2;

  /**
   * Multiple hierarchies error name
   * @see {@link ERROR_CODE_MULTIPLE_HIERARCHIES}
   */
  public static readonly ERROR_NAME_MULTIPLE_HIERARCHIES = 'There are multiple hierarchies.';
  /**
   * The hierarchy contains a loop path error code
   */
  public static readonly ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY + 3;

  /**
   * The hierarchy contains a loop path error name
   * @see {@link ERROR_CODE_HIERARCHY_CONTAINS_LOOP_PATH}
   */
  public static readonly ERROR_NAME_HIERARCHY_CONTAINS_LOOP_PATH = 'The hierarchy contains a loop path.';

  /**
  * The hierarchy contains an employee which as more than one supervisors error code.
  */
  public static readonly ERROR_CODE_HIERARCHY_MULTIPLE_SUPERVISORS =
    ModulesErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY + 4;

  /**
   * The hierarchy contains an employee which as more than one supervisors name
   * @see {@link ERROR_CODE_HIERARCHY_MULTIPLE_SUPERVISORS}
   */
  public static readonly ERROR_NAME_HIERARCHY_MULTIPLE_SUPERVISORS = 'The hierarchy contains an employee which as more than one supervisors';
}

/**
 * The hierarchy is invalid error
 */
export class InvalidHierarchyError extends AppError {
  constructor(stack: ErrorStackObject[], message?: string) {
    super(HierarchyDomainErrorCodes.ERROR_CODE_INVALID_HIERARCHY_CODE,
      HierarchyDomainErrorCodes.ERROR_NAME_INVALID_HIERARCHY,
      message || HierarchyDomainErrorCodes.ERROR_NAME_INVALID_HIERARCHY,
      stack);
  }
}