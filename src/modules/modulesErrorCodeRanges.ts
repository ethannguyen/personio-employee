import { ErrorCodeRanges } from 'src/libs/error';

/**
 * Predefined range for error codes of errors originated in all modules of the application
 * The reserved error code range is from 200000 to 299999.
 */
export class ModulesErrorCodeRanges {
  // Error code range for domain of user module.
  public static ERROR_CODE_RANGE_START_DOMAIN_USER = ErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_LAYER + 0;

  // Error code range for application of user module.
  public static ERROR_CODE_RANGE_START_APPLICATION_USER = ErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_LAYER + 0;

  // Error code range for domain of hierarchy module.
  public static ERROR_CODE_RANGE_START_DOMAIN_HIERARCHY = ErrorCodeRanges.ERROR_CODE_RANGE_START_DOMAIN_LAYER + 1000;

  // Error code range for application of hierarchy module.
  public static ERROR_CODE_RANGE_START_APPLICATION_HIERARCHY = ErrorCodeRanges.ERROR_CODE_RANGE_START_APPLICATION_LAYER + 1000;
}