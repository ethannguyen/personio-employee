import dotenv from 'dotenv';
// Run once while the service is starting to load .env to process.env
const result = dotenv.config();

if (result.error) {
  console.log('Unable to load .env');
  console.log(process.env)
}