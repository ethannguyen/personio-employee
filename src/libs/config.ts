import { MissingConfigurationError } from './error';

export enum NodeEnv {
  Development = 'development',
  Production = 'production'
}

export default {
  port: +(process.env.PORT || 3000),
  nodeEnv: getNodeEnv(process.env.NODE_ENV),
  jwtSecretKey: process.env.JWT_SECRET_KEY,
  apiRoute: getStringConfig('API_ROUTE'),
  swaggerRoute: getStringConfig('SWAGGER_ROUTE'),
  databaseConnectionRetries: +(process.env.DATABASE_CONNECTION_RETRIES || 3),
}

function getNodeEnv(env: string | undefined): NodeEnv {
  if (!env) {
    return NodeEnv.Development;
  }

  return (NodeEnv as any)[env] || NodeEnv.Development
}

function getStringConfig(key: string): string {
  const config = process.env[key];
  if (!config) {
    throw new MissingConfigurationError();
  }

  return config;
}
