import { ErrorObject, ErrorStack, ErrorStackObject } from './errorObject';

/**
 * An error thrown within the whole application 
 * which provides more details than framework's Error @see {@link Error}
 */
export class AppError extends Error {
  /**
   * @inheritdoc {@link ErrorObject}
   */
  public code: number;

  /**
   * @inheritdoc {@link ErrorObject}
   */
  public errorStack?: ErrorStack;

  constructor(code: number, name: string, message: string, errorStack?: ErrorStackObject[]) {
    super(message);
    this.code = code;
    this.name = name;
    this.errorStack = errorStack;
  }

  /**
   * Convert to plain ErrorObject
   */
  toErrorObject(): ErrorObject {
    return {
      code: this.code,
      name: this.name,
      message: this.message,
      errorStack: this.errorStack,
    }
  }

  /**
   * Convert to an error stack which includes also this error and its stack
   */
  toErrorStack(): ErrorStackObject[] {
    return [
      { code: this.code, name: this.name, detail: this.message },
      ...(this.errorStack || []),
    ]
  }
}

/**
 * Verify if the input error is AppError or not.
 * @remarks This help TS determines AppError type in compile time.
 */
export function isAppError(err: any): err is AppError {
  return err instanceof AppError
}