/**
 * The error interface providing more details via its error stack.
 */
export interface ErrorObject {
  /**
   * Indicates the unique identification of the error reason/type
   */
  code: number;

  /**
   * Indicates the information name of the error code
   */
  name: string;

  /**
   * Indicates the description of the error
   */
  message: string;

  /**
   * Error stack providing details of this error
   */
  errorStack?: ErrorStack;
}

/**
 * Represents the object that provides a set of useful information of an error
 */
export interface ErrorStackObject {
  /**
   * Indicates the unique identification of the error reason/type
   */
  code?: number;

  /**
   * Indicates the information name of the error code
   */
  name?: string;

  /**
   * Indicates the details of the error
   */
  detail: string;
}

/**
 * A stack of errors
 */
export type ErrorStack = ErrorStackObject[];