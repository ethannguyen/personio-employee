import { ErrorCodeRanges } from '.';
import { AppError } from './appError'

/**
 * A class contains all predefined errors and names of libs layer
 */
export class LibsErrorCodes {
  /**
   * Missing configuration error code
   */
  public static readonly ERROR_CODE_MISSING_CONFIGURATION =
    ErrorCodeRanges.ERROR_CODE_RANGE_START_LIBS_LAYER + 0;

  /**
   * Missing configuration error name
   * @see {@link ERROR_CODE_MISSING_CONFIGURATION}
   */
  public static readonly ERROR_NAME_MISSING_CONFIGURATION = 'Service\'s configuration missing.';

  /**
   * An error code represents for an unknown error in the whole system.
   */
  public static readonly ERROR_CODE_UNKNOWN = ErrorCodeRanges.ERROR_CODE_RANGE_START_LIBS_LAYER + 99999;

  /**
   * An error name represents for an unknown error in the whole system.
   */
  public static readonly ERROR_NAME_UNKNOWN = "Unknown error";
}

export class MissingConfigurationError extends AppError {
  constructor() {
    super(LibsErrorCodes.ERROR_CODE_MISSING_CONFIGURATION,
      LibsErrorCodes.ERROR_NAME_MISSING_CONFIGURATION,
      'One of service\'s configuration is missing.')
  }
}