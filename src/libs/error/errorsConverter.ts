import { ErrorStackObject } from './errorObject';

/**
 * Convert Nodejs default error type to error stack.
 */
export function toErrorStackObject(error: Error): ErrorStackObject[] {
  if (!error.stack) {
    return [];
  }

  return error.stack.split('\n').map(detail => ({ detail: detail.trim() }));
}