/**
 * Predefined range for error codes of errors originated in the application
 * The reserved error code range for all main layers is from 100000 to 999998.
 */
export class ErrorCodeRanges {
  // Error code range for domain layer errors.
  public static ERROR_CODE_RANGE_START_DOMAIN_LAYER = 100000;

  // Error code range for application layer errors.
  public static ERROR_CODE_RANGE_START_APPLICATION_LAYER = 200000;

  // Error code range for interface layer errors.
  public static ERROR_CODE_RANGE_START_INTERFACE_LAYER = 300000;

  // Error code range for infrastructure layer errors.
  public static ERROR_CODE_RANGE_START_INFRASTRUCTURE_LAYER = 400000;


  // Error code range for libs layer errors.
  public static ERROR_CODE_RANGE_START_LIBS_LAYER = 900000;
}