import { ErrorCodeRanges } from 'src/libs/error';

/**
 * Predefined range for error codes of errors originated in interface layer
 * The reserved error code range is from 300000 to 399999.
 */
export class InterfaceErrorCodeRanges {
  // Error code range for api interface.
  public static ERROR_CODE_RANGE_START_API = ErrorCodeRanges.ERROR_CODE_RANGE_START_INTERFACE_LAYER + 0;
}