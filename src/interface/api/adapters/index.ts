import { container } from 'tsyringe';
import { HierarchyController } from './hierarchy/hierarchyController';
import { SessionController } from './session/sessionController';
import { UserController } from './user';

export function registerAdapterDependencies() {
  container.register(UserController, { useClass: UserController });
  container.register(SessionController, { useClass: SessionController });
  container.register(HierarchyController, { useClass: HierarchyController });
}