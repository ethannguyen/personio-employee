import { Request, Response } from 'express';
import { inject, injectable } from 'tsyringe';
import { UserCreationBodyValidator } from './userValidators';
import { UserCreationBody } from './types/userCreationBody';
import { IUserService, UserExistsError, UserServiceToken } from 'src/modules/user';
import { BaseController } from '../../baseController';
import { catchAllErrors, handleRequestValidationResults } from '../../middleware';
import { responseWith200Success, responseWith409Conflict } from '../../utilities';

/**
 * An express controller which handles route to user resource
 */
@injectable()
export class UserController extends BaseController {
  private userService: IUserService;

  constructor(@inject(UserServiceToken) userService: IUserService) {
    super();
    this.userService = userService;

    this.router.post('/',
      UserCreationBodyValidator,
      handleRequestValidationResults,
      catchAllErrors(this.createUser.bind(this)));
  }

  private async createUser(request: Request<unknown, unknown, UserCreationBody>, response: Response): Promise<void> {
    const { userName, password } = request.body;
    try {
      await this.userService.createUser(userName, password);
      responseWith200Success(response);
    } catch (err) {
      if (err instanceof UserExistsError) {
        return responseWith409Conflict(response, err);
      }

      throw err;
    }
  }
}