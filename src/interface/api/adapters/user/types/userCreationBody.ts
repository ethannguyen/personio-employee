/**
 * The body of a new user creation request
 * Schema follows the API specification {@see src/swagger.yaml}
 */
export interface UserCreationBody {
  /**
   * User's name, is the identifier of a user
   */
  userName: string;

  /**
   * User's password, used for authentication
   */
  password: string;
}