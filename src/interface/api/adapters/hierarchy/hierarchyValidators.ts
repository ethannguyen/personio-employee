import { checkSchema } from 'express-validator';

export const GettingEmployeeValidator = checkSchema({
  employeeName: {
    in: 'params',
    isString: true,
    isLength: { options: { max: 255 } },
  },
  supervisorDepth: {
    in: 'query',
    isInt: { options: { min: 0 } },
    optional: true,
  },
});