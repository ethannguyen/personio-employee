/**
 * Represent a pair employee and supervisor
 * where the employee is supervised by the supervisor
 */
export interface EmployeeSupervisorPair {
  employee: string;
  supervisor: string;
}