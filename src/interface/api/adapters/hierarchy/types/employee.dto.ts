import { Employee } from 'src/modules/hierarchy';

/**
 * Represent an employee in the system.
 * Schema follows the API specification {@see src/swagger.yaml}
 */
export interface EmployeeDto {
  name: string;
  supervisor?: EmployeeDto;
}

/**
 * Convert Employee Domain to Employee Dto
 */
export function fromEmployeeToDto(employee: Employee): EmployeeDto {
  const employeeDto: EmployeeDto = { name: employee.name }

  if (employee.supervisor) {
    employeeDto.supervisor = fromEmployeeToDto(employee.supervisor);
  }

  return employeeDto;
}