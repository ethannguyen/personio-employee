/**
 * A DTO represent for a hierarchy in the system.
 * Schema follows the API specification {@see src/swagger.yaml}
 */
export interface HierarchyDto {
  [key: string]: HierarchyDto | {};
}