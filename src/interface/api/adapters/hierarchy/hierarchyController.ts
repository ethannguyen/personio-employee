import express, { Request, Response } from 'express';
import { inject, injectable } from 'tsyringe';
import clarinet from 'clarinet';

import { AppError } from 'src/libs/error';
import { Employee, EmployeeNotFoundInHierarchyError, Hierarchy, HierarchyNotFoundError, HierarchyServiceToken, IHierarchyService } from 'src/modules/hierarchy';
import { BaseController } from '../../baseController';
import { catchAllErrors, handleRequestValidationResults } from '../../middleware';
import { ApiErrorCodes } from '../../apiErrorCodes';
import { responseWith200Success, responseWith400BadRequest, responseWith404NotFound } from '../../utilities';
import { EmployeeSupervisorPair } from './types/employeeSupervisorPair';
import { HierarchyDto } from './types/hierarchy.dto';
import { GettingEmployeeValidator } from './hierarchyValidators';
import { fromEmployeeToDto } from './types/employee.dto';

/**
 * An express controller which handles route to session resource
 */
@injectable()
export class HierarchyController extends BaseController {
  private hierarchyService: IHierarchyService;

  constructor(@inject(HierarchyServiceToken) hierarchyService: IHierarchyService) {
    super();
    this.hierarchyService = hierarchyService;

    // Json parser by default remove duplicated key,
    // this is not suitable for creating a hierarchy while we supports duplicated key.
    // Therefore, we use the custom parse to keep the duplicated key
    this.router.post('/',
      express.raw({ type: 'application/json' }),
      catchAllErrors(this.createHierarchy.bind(this)));

    this.router.get('/employees/:employeeName',
      express.json(),
      GettingEmployeeValidator,
      handleRequestValidationResults,
      catchAllErrors(this.getEmployeeInHierarchy.bind(this)));
  }

  private async createHierarchy(request: Request<unknown, unknown, Buffer>, response: Response): Promise<void> {
    try {
      const employeeSupervisorPairs = this.getHierarchyBody(request.body.toString());
      const employeesHasSupervisor = employeeSupervisorPairs.map(pair => new Employee(pair.employee));

      const employeesWithoutSupervisor = employeeSupervisorPairs
        .filter(pair => !employeesHasSupervisor.find(employee => employee.name === pair.supervisor))
        .map(pair => new Employee(pair.supervisor));

      const allEmployees = [...employeesHasSupervisor, ...employeesWithoutSupervisor];
      employeesHasSupervisor.forEach((employee, index) => {
        const supervisorName = employeeSupervisorPairs[index].supervisor;
        // At this point, supervisor is always available
        employee.supervisedBy(new Employee(supervisorName));
      });

      const hierarchy = await this.hierarchyService.create(allEmployees);
      const hierarchyResponseModel = this.convertToHierarchyResponseModel(hierarchy);
      responseWith200Success(response, hierarchyResponseModel);
    } catch (err) {
      if (err instanceof AppError) {
        responseWith400BadRequest(response, err.toErrorObject());
        return;
      }

      throw err;
    }
  }

  private async getEmployeeInHierarchy(request: Request, response: Response): Promise<void> {
    try {
      const { employeeName } = request.params;
      const supervisorDepth = request.query.supervisorDepth ? +(request.query.supervisorDepth) : 2;
      const employee = await this.hierarchyService.getEmployeeInHierarchy(employeeName, supervisorDepth);
      responseWith200Success(response, fromEmployeeToDto(employee));
    } catch (err) {
      if (err instanceof HierarchyNotFoundError || err instanceof EmployeeNotFoundInHierarchyError) {
        responseWith404NotFound(response, err.toErrorObject());
        return;
      }

      throw err;
    }
  }

  /**
   * Parse JSON and convert input JSON key/value as employee/supervisor
   * to an array of {@link EmployeeSupervisorPair}
   * By doing this, we can keep the duplicated employee/supervisor pair
   */
  private getHierarchyBody(body: string): EmployeeSupervisorPair[] {
    const hierarchy: EmployeeSupervisorPair[] = [];
    const parser = clarinet.parser();

    parser.onopenobject = parser.onkey = (key: string) => {
      hierarchy.push({ employee: key, supervisor: '' });
    };

    parser.onvalue = (value: unknown) => {
      if (typeof value !== 'string') {
        throw new AppError(
          ApiErrorCodes.ERROR_CODE_INVALID_HTTP_REQUEST,
          ApiErrorCodes.ERROR_NAME_INVALID_HTTP_REQUEST,
          'Parameters or body schema of the request is invalid',
        );
      }

      hierarchy[hierarchy.length - 1].supervisor = value;
    };

    try {
      parser.write(body);
      parser.close();
    } catch (err) {
      throw new AppError(
        ApiErrorCodes.ERROR_CODE_INVALID_HTTP_REQUEST,
        ApiErrorCodes.ERROR_NAME_INVALID_HTTP_REQUEST,
        (err as Error).message,
      );
    }

    return hierarchy;
  }

  private convertToHierarchyResponseModel(hierarchy: Hierarchy, employee?: Employee): HierarchyDto {
    const currentEmployee = employee || hierarchy.headEmployee;
    const supervisees = hierarchy.getSupervisees(currentEmployee);
    const employeeModel = supervisees.reduce((acc, supervisee) => {
      acc[supervisee.name] = this.convertToHierarchyResponseModel(hierarchy, supervisee);
      return acc;
    }, {} as HierarchyDto);

    // Return hierarchy if the current employee is head employee.
    if (!employee) {
      const hierarchyModel: HierarchyDto = {};
      hierarchyModel[currentEmployee.name] = employeeModel;
      return hierarchyModel;
    }

    return employeeModel;
  }
}