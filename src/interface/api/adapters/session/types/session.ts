/**
 * The session in the system. With a session is available, all authenticated resources can be accessed.
 * Schema follows the API specification {@see src/swagger.yaml}
 */
export interface Session {
  /**
   * A JWT token can be added to authentication header to access authenticated resources.
   */
  token: string;
}