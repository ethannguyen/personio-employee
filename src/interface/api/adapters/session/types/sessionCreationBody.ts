/**
 * The body of a session creation request
 * Schema follows the API specification {@see src/swagger.yaml}
 */
export interface SessionCreationBody {
  /**
   * Identifier of a user
   */
  userName: string;

  /**
   * Password used for authentication
   */
  password: string;
}