import { Request, Response } from 'express';
import { inject, injectable } from 'tsyringe';
import jwt from 'jsonwebtoken';
import {
  IUserService, UnauthorizedUserError,
  UserNotFoundError, UserServiceToken
} from 'src/modules/user';
import { BaseController } from '../../baseController';
import { catchAllErrors, handleRequestValidationResults } from '../../middleware';
import {
  responseWith200Success, responseWith403Unauthorized,
  responseWith404NotFound, responseWith500InternalServerError
} from '../../utilities';
import { SessionCreationBody } from './types/sessionCreationBody';
import { SessionCreationBodyValidator } from './sessionValidators';
import { User } from 'src/modules/user/domain/user';
import config from 'src/libs/config';
import { MissingConfigurationError } from 'src/libs/error';
import { Session } from './types/session';

/**
 * An express controller which handles route to session resource
 */
@injectable()
export class SessionController extends BaseController {
  private userService: IUserService;

  constructor(@inject(UserServiceToken) userService: IUserService) {
    super();
    this.userService = userService;

    this.router.post('/',
      SessionCreationBodyValidator,
      handleRequestValidationResults,
      catchAllErrors(this.createSession.bind(this)));
  }

  private async createSession(request: Request<unknown, unknown, SessionCreationBody>, response: Response): Promise<void> {
    const { userName, password } = request.body;
    try {
      const user = await this.userService.authenticateUser(userName, password);
      const sessionToken = this.createSessionToken(user);
      const session: Session = { token: sessionToken };
      responseWith200Success(response, session);
    } catch (err) {
      if (err instanceof UserNotFoundError) {
        return responseWith404NotFound(response, err);
      }

      if (err instanceof UnauthorizedUserError) {
        return responseWith403Unauthorized(response, err);
      }

      if (err instanceof MissingConfigurationError) {
        return responseWith500InternalServerError(response, err);
      }

      throw err;
    }
  }

  private createSessionToken(user: User): string {
    const { jwtSecretKey } = config;

    if (!jwtSecretKey) {
      throw new MissingConfigurationError();
    }

    return jwt.sign(
      { userName: user.userName },
      jwtSecretKey,
      {
        expiresIn: "2h",
      }
    );
  }
}