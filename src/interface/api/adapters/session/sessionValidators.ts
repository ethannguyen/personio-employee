import { checkSchema } from 'express-validator';

export const SessionCreationBodyValidator = checkSchema({
  userName: {
    in: 'body',
    isString: true,
    isLength: { options: { max: 255 } },
  },
  password: {
    in: 'body',
    isString: true,
    isLength: { options: { max: 255 } },
  },
});