import { InterfaceErrorCodeRanges } from '../interfaceErrorCodeRanges';

/**
 * A class contains all predefined errors and names of api in interface layer
 */
export class ApiErrorCodes {
  /**
   * Invalid HTTP request error code
   */
  public static readonly ERROR_CODE_INVALID_HTTP_REQUEST =
    InterfaceErrorCodeRanges.ERROR_CODE_RANGE_START_API + 0;

  /**
   * Invalid HTTP request error name
   * @see {@link ERROR_CODE_INVALID_HTTP_REQUEST}
   */
  public static readonly ERROR_NAME_INVALID_HTTP_REQUEST = 'Invalid request.';


  /**
   * Invalid body of HTTP request error code
   */
  public static readonly ERROR_CODE_INVALID_BODY_HTTP__REQUEST =
    InterfaceErrorCodeRanges.ERROR_CODE_RANGE_START_API + 1;

  /**
   * Invalid body HTTP request error name
   * @see {@link ERROR_CODE_INVALID_BODY_OF_HTTP__REQUEST}
   */
  public static readonly ERROR_NAME_INVALID_BODY_HTTP_REQUEST = 'Invalid request\'s body.';


  /**
   * Forbidden HTTP request error code
   */
  public static readonly ERROR_CODE_FORBIDDEN_HTTP_REQUEST =
    InterfaceErrorCodeRanges.ERROR_CODE_RANGE_START_API + 2;

  /**
   * Forbidden HTTP request error name
   * @see {@link ERROR_CODE_FORBIDDEN_HTTP_REQUEST}
   */
  public static readonly ERROR_NAME_FORBIDDEN_HTTP_REQUEST = 'Forbidden request.';


  /**
   * Unauthorized HTTP request error code
   */
  public static readonly ERROR_CODE_UNAUTHORIZED_HTTP_REQUEST =
    InterfaceErrorCodeRanges.ERROR_CODE_RANGE_START_API + 3;

  /**
   * Unauthorized HTTP request error name
   * @see {@link ERROR_CODE_UNAUTHORIZED_HTTP_REQUEST}
   */
  public static readonly ERROR_NAME_UNAUTHORIZED_HTTP_REQUEST = 'Unauthorized request.';

  /**
   * Resource not found error code
   */
  public static readonly ERROR_CODE_RESOURCE_NOT_FOUND =
    InterfaceErrorCodeRanges.ERROR_CODE_RANGE_START_API + 4;

  /**
   * Resource not found error name
   * @see {@link ERROR_CODE_RESOURCE_NOT_FOUND}
   */
  public static readonly ERROR_NAME_RESOURCE_NOT_FOUND = 'Resource not found.';
}