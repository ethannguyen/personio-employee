import { registerAdapterDependencies } from './adapters';

export function registerApiDependencies() {
  registerAdapterDependencies();
}

export { Request, Response } from 'express';

export {
  responseWith200Success,
  responseWith409Conflict, responseWith500InternalServerError
} from './utilities';

export { BaseController } from './baseController';

export { handleRequestValidationResults, catchAllErrors } from './middleware';

export { Api } from './api';