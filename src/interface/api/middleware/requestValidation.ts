import { Request, Response, NextFunction } from 'express';
import { validationResult, Location } from 'express-validator';
import { ErrorObject, ErrorStackObject } from 'src/libs/error';
import { HttpStatusCodes } from 'src/libs/http';
import { ApiErrorCodes } from '../apiErrorCodes';

/**
 * An express middleware to handle validation result of a request
 */
export function handleRequestValidationResults(req: Request, res: Response, next: NextFunction) {
  const errors = validationResult(req);

  if (errors.isEmpty()) {
    return next();
  }

  const error: ErrorObject = {
    code: ApiErrorCodes.ERROR_CODE_INVALID_HTTP_REQUEST,
    name: ApiErrorCodes.ERROR_NAME_INVALID_HTTP_REQUEST,
    message: 'Parameters or body schema of the request is invalid',
    errorStack: errors.array().map((validationError): ErrorStackObject => {
      const { code, name } = getErrorInfoFromLocation(validationError.location);
      return {
        code,
        name,
        detail: `${validationError.param} in ${validationError.location}: ${validationError.msg}`
      }
    }),
  }

  return res
    .status(HttpStatusCodes.BAD_REQUEST)
    .json(error);
}

function getErrorInfoFromLocation(location: Location | undefined): { code?: number, name?: string } {
  const defaultErrorInfo = {};

  if (!location) {
    return defaultErrorInfo;
  }

  switch (location) {
    case 'body':
      return {
        code: ApiErrorCodes.ERROR_CODE_INVALID_BODY_HTTP__REQUEST,
        name: ApiErrorCodes.ERROR_NAME_INVALID_BODY_HTTP_REQUEST,
      };
    default:
      return defaultErrorInfo;
  }
}

