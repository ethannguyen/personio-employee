import { Request, Response } from 'express';
import { HttpStatusCodes } from 'src/libs/http';

/**
 * A middleware handles request to invalid route of the api
 */
export function handleInvalidRoute(_request: Request, response: Response) {
  return response.status(HttpStatusCodes.NOT_FOUND).json()
}