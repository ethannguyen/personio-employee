import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import config, { NodeEnv } from 'src/libs/config';
import { ErrorObject, LibsErrorCodes } from 'src/libs/error';
import { HttpStatusCodes } from 'src/libs/http';
import { ApiErrorCodes } from '../apiErrorCodes';

/**
 * A middleware used to authenticate access token of the request
 */
export function authenticate(request: Request, response: Response, next: NextFunction) {
  const token = getTokenFromAuthenticationHeader(request);

  if (!token) {

    const missingTokenError: ErrorObject = {
      code: ApiErrorCodes.ERROR_CODE_FORBIDDEN_HTTP_REQUEST,
      name: ApiErrorCodes.ERROR_NAME_FORBIDDEN_HTTP_REQUEST,
      message: 'Access token is required for authentication.',
    }

    return response
      .status(HttpStatusCodes.FORBIDDEN)
      .json(missingTokenError);
  }

  const { jwtSecretKey } = config;

  if (!jwtSecretKey) {
    const errorMessage = config.nodeEnv === NodeEnv.Development
      ? 'JwtSecretKey configuration is missing.' : 'One of service\'s configuration is missing.'

    const missingKeyError: ErrorObject = {
      code: LibsErrorCodes.ERROR_CODE_MISSING_CONFIGURATION,
      name: LibsErrorCodes.ERROR_NAME_MISSING_CONFIGURATION,
      message: errorMessage,
    }

    return response
      .status(HttpStatusCodes.INTERNAL_SERVER_ERROR)
      .json(missingKeyError);
  }

  try {
    jwt.verify(token, jwtSecretKey);
    return next();
  } catch (err) {
    const unauthorizedError: ErrorObject = {
      code: ApiErrorCodes.ERROR_CODE_UNAUTHORIZED_HTTP_REQUEST,
      name: ApiErrorCodes.ERROR_NAME_UNAUTHORIZED_HTTP_REQUEST,
      message: (err as Error).message,
    }

    return response.status(HttpStatusCodes.UNAUTHORIZED)
      .json(unauthorizedError)
  }
}

function getTokenFromAuthenticationHeader(request: Request): string | undefined {
  const BEARER_PREFIX = 'Bearer ';
  const authorizationHeader = request.headers['authorization'];

  if (!authorizationHeader) {
    return undefined;
  }

  if (!authorizationHeader.startsWith(BEARER_PREFIX)) {
    return undefined;
  }

  return authorizationHeader.substring(BEARER_PREFIX.length, authorizationHeader.length);
}