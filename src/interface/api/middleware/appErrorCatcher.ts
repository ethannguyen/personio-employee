import { NextFunction, Request, Response } from 'express';

/**
 * Catch all error thrown from wrappee function and delegate to the next function
 */
export function catchAllErrors(func: (request: Request, response: Response) => Promise<unknown>) {
  return async (request: Request, response: Response, next: NextFunction) => {
    try {
      await func(request, response);
    } catch (error) {
      next(error);
    }
  };
}