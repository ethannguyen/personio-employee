import { NextFunction, Request, Response } from 'express';
import config, { NodeEnv } from 'src/libs/config';
import {
  AppError, ErrorObject, LibsErrorCodes, toErrorStackObject,
} from 'src/libs/error';
import { HttpStatusCodes } from 'src/libs/http';

export function handleUnhandledError(error: Error, _request: Request, response: Response, _next: NextFunction) {
  if (error instanceof AppError) {
    return response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json(error.toErrorObject());
  }

  const unknownError: ErrorObject = {
    code: LibsErrorCodes.ERROR_CODE_UNKNOWN,
    name: LibsErrorCodes.ERROR_NAME_UNKNOWN,
    message: "Unexpected error occurs.",
    errorStack: config.nodeEnv === NodeEnv.Development ? toErrorStackObject(error) : [],
  };

  return response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json(unknownError);
}