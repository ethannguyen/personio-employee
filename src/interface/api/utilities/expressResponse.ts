import { Response } from 'express';
import { ErrorObject } from 'src/libs/error';
import { HttpStatusCodes } from 'src/libs/http';

export function responseWith200Success(response: Response, data?: unknown) {
  response.status(HttpStatusCodes.OK).json(data);
}

export function responseWith400BadRequest(response: Response, error: ErrorObject) {
  response.status(HttpStatusCodes.BAD_REQUEST).json(error);
}

export function responseWith404NotFound(response: Response, error: ErrorObject) {
  response.status(HttpStatusCodes.NOT_FOUND).json(error);
}

export function responseWith403Unauthorized(response: Response, error: ErrorObject) {
  response.status(HttpStatusCodes.UNAUTHORIZED).json(error);
}

export function responseWith409Conflict(response: Response, error: ErrorObject) {
  response.status(HttpStatusCodes.CONFLICT).json(error);
}

export function responseWith500InternalServerError(response: Response, error: ErrorObject) {
  response.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).json(error);
}