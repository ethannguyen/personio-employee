import cors from 'cors';
import express, { Router } from 'express';
import yamljs from 'yamljs';
import swaggerUi from 'swagger-ui-express';
import { authenticate, handleInvalidRoute, handleUnhandledError } from './middleware';
import { container } from 'tsyringe';
import config from 'src/libs/config';
import { Server } from 'http';
import { UserController } from './adapters/user';
import { SessionController } from './adapters/session/sessionController';
import { HierarchyController } from './adapters/hierarchy/hierarchyController';

export class Api {
  private static readonly SWAGGER_YAML_LOCATION: string = './swagger.yaml';

  private readonly expressApp = express();

  public setup() {
    this.expressApp.use(cors());
    this.expressApp.use(express.urlencoded({ extended: true }));
    this.setupSwaggerUi();
    this.expressApp.use(config.apiRoute, this.apiRoutes());
    this.expressApp.use(handleUnhandledError);
    this.expressApp.use('*', handleInvalidRoute);
  }

  public start(): Promise<Server> {
    return new Promise((resolve) => {
      const server = this.expressApp.listen(config.port, () => {
        console.log(`App is listening on port ${config.port}`);
        server.keepAliveTimeout = 65 * 1000;
        server.headersTimeout = 66 * 1000;
        resolve(server);
      });
    });
  }

  private setupSwaggerUi() {
    const swaggerDocument = yamljs.load(Api.SWAGGER_YAML_LOCATION);
    this.expressApp.use(config.swaggerRoute, swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }

  private apiRoutes(): Router {
    const router = Router();
    const userController = container.resolve(UserController);
    const sessionController = container.resolve(SessionController);
    const hierarchy = container.resolve(HierarchyController);
    router.use('/users', express.json(), userController.router);
    router.use('/sessions', express.json(), sessionController.router);
    router.use('/hierarchy', authenticate, hierarchy.router);
    return router;
  }
}