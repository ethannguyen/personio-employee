import { Server } from 'http';
import {
  connectDatabase, disconnectDatabase,
  registerDatabaseDependencies
} from './infrastructure/database';
import { Api, registerApiDependencies } from './interface/api';
import { registerApplicationDependencies } from './modules';

/**
 * The entry point of the whole application.
 * Its responsibility is starting/stopping the whole application.
 */
export class Application {
  private readonly api = new Api();

  /**
   * Start the application
   * @returns Instance of express server
   */
  public async start(): Promise<Server> {
    await connectDatabase();
    this.buildAppIocContainer();
    this.api.setup();
    return this.api.start();
  }

  /**
   * Stop the application
   */
  public async stop(): Promise<void> {
    await disconnectDatabase();
  }

  private buildAppIocContainer() {
    registerDatabaseDependencies();
    registerApiDependencies();
    registerApplicationDependencies();
  }
}