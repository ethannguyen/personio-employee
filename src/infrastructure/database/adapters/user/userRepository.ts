import { AbstractRepository, EntityRepository } from 'typeorm';
import { User, IUserRepository } from 'src/modules/user';
import { UserEntity } from './user.orm';

@EntityRepository(UserEntity)
export class UserRepository extends AbstractRepository<UserEntity> implements IUserRepository {

  /**
   * @inheritdoc {@link IUserRepository}
   */
  public async createUser(user: User): Promise<void> {
    const insertResult = await this.repository.insert({
      userName: user.userName,
      password: user.password
    });

    if (insertResult.identifiers.length !== 1) {
      throw new Error('User is not created');
    }
  }

  /**
   * @inheritdoc {@link IUserRepository}
   */
  public async findUser(userName: string): Promise<User | undefined> {
    const userEntity = await this.repository.findOne({ where: { userName } });
    return userEntity ? userEntity.toUser() : undefined;
  }
}