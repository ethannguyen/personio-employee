import { User } from 'src/modules/user';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

/**
 * Represent a user ORM entity that associates with @see {@link User} domain concept
 */
@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'smallint' })
  public id?: number;

  @Column({ name: 'user_name', type: 'varchar', length: 255, nullable: false, unique: true })
  public userName: string;

  @Column({ name: 'password', type: 'varchar', length: 255, nullable: false })
  public password: string;

  constructor(userName: string, password: string) {
    this.userName = userName;
    this.password = password;
  }

  toUser(): User {
    return new User(this.userName, this.password);
  }
}
