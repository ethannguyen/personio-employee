import { container } from 'tsyringe';
import { getCustomRepository } from 'typeorm';
import { IUserRepository, UserRepositoryToken } from 'src/modules/user';
import { UserRepository } from './user/userRepository';
import { HierarchyRepositoryToken, IHierarchyRepository } from 'src/modules/hierarchy';
import { HierarchyRepository } from './hierarchy/hierarchyRepository';

export function registerAdapterDependencies() {
  container.register<IUserRepository>(UserRepositoryToken, {
    useFactory: () => {
      return getCustomRepository(UserRepository);
    }
  });

  container.register<IHierarchyRepository>(HierarchyRepositoryToken, {
    useFactory: () => {
      return getCustomRepository(HierarchyRepository);
    }
  });
}