import { Employee } from 'src/modules/hierarchy';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinTable, JoinColumn, Tree, TreeParent, TreeChildren } from 'typeorm';

/**
 * Represent an employee ORM entity that associates with @see {@link Employee} domain concept
 */
@Entity('employee')
@Tree('closure-table', {
  ancestorColumnName: (column) => "ancestor_" + column.propertyName,
  descendantColumnName: (column) => "descendant_" + column.propertyName,
})
export class EmployeeEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  public id?: number;

  @Column({ name: 'name', type: 'varchar', length: 255, nullable: false, unique: true })
  public name: string;

  @TreeParent({ onDelete: "CASCADE" })
  public supervisor?: EmployeeEntity;

  @TreeChildren({ cascade: ['remove'] })
  public supervisees: EmployeeEntity[] | undefined;

  constructor(name: string) {
    this.name = name;
  }

  toEmployee(): Employee {
    const employee = new Employee(this.name);
    if (this.supervisor) {
      employee.supervisedBy(this.supervisor.toEmployee());
    }

    return employee;
  }
}
