import { Hierarchy, HierarchyNotFoundError, IHierarchyRepository } from 'src/modules/hierarchy';
import { AbstractRepository, EntityManager, EntityRepository, QueryRunner } from 'typeorm';
import { EmployeeEntity } from './employee.orm';

@EntityRepository(EmployeeEntity)
export class HierarchyRepository extends AbstractRepository<EmployeeEntity> implements IHierarchyRepository {

  /**
  * @inheritdoc IHierarchyRepository
  */
  public async createHierarchy(hierarchy: Hierarchy): Promise<void> {
    const headEmployee = hierarchy.headEmployee;
    const headEmployeeEntity = new EmployeeEntity(headEmployee.name);

    const currentHierarchies = await this.treeRepository.findTrees();
    const queryRunner = this.manager.connection.createQueryRunner();
    await queryRunner.startTransaction();
    const manager = queryRunner.manager;

    try {
      await Promise.all(currentHierarchies.map(currentHierarchy =>
        manager.remove(currentHierarchy)));
      await manager.save(headEmployeeEntity);
      await this.createSupervisees(headEmployeeEntity, hierarchy, manager);
      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
  }

  /**
   * @inheritdoc IHierarchyRepository
   */
  public async getCurrentHierarchy(): Promise<Hierarchy> {
    const hierarchies = await this.treeRepository.findTrees();

    if (hierarchies.length === 0) {
      throw new HierarchyNotFoundError();
    }

    const employees = this.getEmployees(hierarchies[0]);
    const hierarchy = Hierarchy.create(employees.map(e => e.toEmployee()));
    return hierarchy;
  }

  private async createSupervisees(employeeEntity: EmployeeEntity, hierarchy: Hierarchy, manager: EntityManager) {
    const supervisees = hierarchy.getSupervisees(employeeEntity.toEmployee());
    const superviseeEntities = supervisees.map(supervisee => {
      const superviseeEntity = new EmployeeEntity(supervisee.name);
      superviseeEntity.supervisor = employeeEntity;
      return superviseeEntity;
    });

    for (let superviseeEntity of superviseeEntities) {
      await manager.save(superviseeEntity);
      await this.createSupervisees(superviseeEntity, hierarchy, manager);
    }
  }

  private getEmployees(employee: EmployeeEntity, employees?: EmployeeEntity[]): EmployeeEntity[] {
    let newEmployeeList = [...(employees || []), employee];
    const supervisees = employee.supervisees || [];

    supervisees.forEach(supervisee => {
      supervisee.supervisor = employee;
      newEmployeeList = this.getEmployees(supervisee, newEmployeeList);
    });

    return newEmployeeList;
  }
}