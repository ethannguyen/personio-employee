import config from 'src/libs/config';
import { Connection, ConnectionOptions, createConnection, getConnection, getConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

/**
 * Connect to the application's database
 */
export async function connectDatabase(): Promise<Connection> {
  const connectionOptions = await getConnectionOptions();
  const retries = config.databaseConnectionRetries;
  let attempt = 0;
  let connection: Connection | undefined = undefined;

  while (true) {
    try {
      connection = await createConnection({
        ...connectionOptions,
        namingStrategy: new SnakeNamingStrategy(),
      });
      console.info(`Database connected`);
      return connection;
    } catch (err) {
      if (attempt++ === retries) {
        throw err;
      }
      console.warn(`Attempt to connect to database ${attempt} time(s)`);
      await new Promise<void>((resolve) => setTimeout(() => { resolve() }, 10000));
    }
  }
}

/**
 * Disconnect from the application's database
 */
export async function disconnectDatabase(): Promise<void> {
  return getConnection().close();
}
