import { registerAdapterDependencies } from './adapters';

export function registerDatabaseDependencies() {
  registerAdapterDependencies();
}

export { connectDatabase, disconnectDatabase } from './databaseConnection';
