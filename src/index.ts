import 'reflect-metadata';
import './libs/env';
import { Application } from './app';
import { Server } from 'http';

const application = new Application();

application
  .start()
  .then((server: Server) => {
    server.on('error', handleExit);

    process.on('SIGINT', () => {
      console.info('End process: SIGINT');
      server.close(handleExit);
    });
  })
  .catch((err) => {
    console.error('FATAL ERROR', err);
    process.exit(1);
  });

/**
 * Handle existing process of the application
 * @param error The error could be thrown from the application
 */
function handleExit(error?: Error) {
  if (error) {
    console.error('FATAL ERROR', error);
  }

  console.log('App is stopping');

  application
    .stop()
    .then(() => {
      process.exit(error ? 1 : 0);
    })
    .catch((err: Error) => {
      console.error(err);
      process.exit(1);
    });
}