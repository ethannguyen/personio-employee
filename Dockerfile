FROM node:12.22-alpine

WORKDIR /usr/personio-employee

# Define all args
ARG LOG_LEVEL=error
ARG NODE_ENV
ARG PORT

# Define all env
ENV NPM_CONFIG_LOGLEVEL ${LOG_LEVEL}
ENV NODE_ENV ${NODE_ENV}
ENV PORT ${PORT}

# Build dependencies, see https://github.com/kelektiv/node.bcrypt.js/wiki/Installation-Instructions#alpine-linux-based-images
RUN apk --no-cache add --update --no-progress --virtual builds-deps build-base python3
COPY package*.json .
RUN npm install
RUN npm rebuild bcrypt --build-from-source

# Build personio-employee service
COPY . .
RUN npm run build:prod
EXPOSE ${PORT}

WORKDIR /usr/personio-employee/dist

# Run service
CMD ["node", "." ]
