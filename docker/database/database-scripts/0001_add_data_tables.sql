USE personio_employee;

CALL upgrade_database(1, 0, 0, 1, 1, 0, 
'Add user tables', 
'CREATE TABLE user (id INT NOT NULL AUTO_INCREMENT, user_name VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY (id), UNIQUE KEY (user_name));');

CALL upgrade_database(1, 1, 0, 1, 2, 0, 
'Add employee table', 
'CREATE TABLE employee (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(255) NOT NULL, supervisor_id INT, PRIMARY KEY (id), FOREIGN KEY (supervisor_id) REFERENCES employee(id) ON DELETE CASCADE ON UPDATE CASCADE, UNIQUE KEY (name));');

CALL upgrade_database(1, 2, 0, 1, 3, 0, 
'Add employee_closure table', 
'CREATE TABLE employee_closure (ancestor_Id INT NOT NULL, descendant_Id INT NOT NULL, depth SMALLINT(5) NOT NULL DEFAULT 0, PRIMARY KEY(ancestor_Id, descendant_Id))');

