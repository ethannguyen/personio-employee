CREATE DATABASE IF NOT EXISTS personio_employee;

USE personio_employee;

DROP PROCEDURE IF EXISTS create_version_table_if_not_exists;
DROP PROCEDURE IF EXISTS upgrade_database;

DELIMITER @@

CREATE PROCEDURE create_version_table_if_not_exists() 
BEGIN
  SET @version_table_exists := (
      SELECT COUNT(*)F
      FROM information_schema.tables
      WHERE table_schema = DATABASE() AND table_name = 'version'
    );

  IF @version_table_exists = 0
  THEN
    CREATE TABLE version (
      id SMALLINT NOT NULL AUTO_INCREMENT,
      major TINYINT NOT NULL,
      minor TINYINT NOT NULL,
      patch SMALLINT NOT NULL,
      description VARCHAR(255),
      PRIMARY KEY (id)
    );

    INSERT INTO version (major, minor, patch, description) VALUES (1, 0, 0, 'Initial database');
  END IF;
END;

CREATE PROCEDURE upgrade_database(
  IN expected_major TINYINT,
  IN expected_minor TINYINT,
  IN expected_patch SMALLINT,
  IN new_major TINYINT,
  IN new_minor TINYINT,
  IN new_patch SMALLINT,
  IN description VARCHAR(255),
  IN update_script TEXT
)
BEGIN
  DECLARE current_major TINYINT;
  DECLARE current_minor TINYINT;
  DECLARE current_patch SMALLINT;
  
  DECLARE errno INT;
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    GET CURRENT DIAGNOSTICS CONDITION 1 errno = MYSQL_ERRNO;
    SELECT errno AS MYSQL_ERROR;
    ROLLBACK;
  END;


  SELECT major, minor, patch 
  INTO current_major, current_minor, current_patch
  FROM version ORDER BY id DESC LIMIT 1;

  SELECT CONCAT('Current version: ', current_major, '.', current_minor, '.', current_patch);

  SELECT update_script;

  IF expected_major = current_major AND expected_minor = current_minor AND expected_patch = current_patch THEN
    START TRANSACTION;
      SET @script = update_script;
      PREPARE stmt FROM @script;
      EXECUTE stmt;
      DEALLOCATE PREPARE stmt;
      INSERT INTO version (major, minor, patch, description) VALUES (new_major, new_minor, new_patch, description);
    COMMIT;
    SELECT 'Upgrade database successfully';
  ELSEIF (expected_major < current_major OR expected_minor < current_minor OR expected_patch < current_patch) THEN
    SELECT 'Skip upgrading database because current version is higher';
  ELSE
    SELECT CONCAT('Invalid database version for upgrading, expected version: ', expected_major, '.', expected_minor, '.', expected_patch);
  END IF;
END;
@@
DELIMITER ;

CALL create_version_table_if_not_exists();
DROP PROCEDURE create_version_table_if_not_exists;